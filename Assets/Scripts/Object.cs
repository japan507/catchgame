﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private BucketColor color;

    [SerializeField]
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (GameStats.gameState)
        {
            case GameState.Play:
                transform.position -= new Vector3(0, speed) * Time.deltaTime;
                break;
            case GameState.Result:
                Destroy(gameObject);
                break;
        }
        
    }

    public BucketColor getColor()
    {
        return color;
    }

    public int getScore()
    {
        return score;
    }
}
