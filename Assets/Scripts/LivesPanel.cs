﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesPanel : MonoBehaviour
{
    [SerializeField]
    private Transform livesIcon;

    [SerializeField]
    private GameStats gameStats;

    // Start is called before the first frame update
    void Start()
    {
        //spawn heart icon
        for(int i = 0; i < gameStats.getLives(); i++)
        {
            Instantiate(livesIcon, transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void lostLife()
    {
        //destroy heart icon
        Destroy(transform.GetChild(transform.childCount - 1).gameObject);
    }
}
