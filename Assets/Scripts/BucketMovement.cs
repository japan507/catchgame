﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BucketMovement : MonoBehaviour
{
    private int targetLane;

    private int LANE_DISTANCE = 2;

    private Vector3 targetPosition;

    // Start is called before the first frame update
    void Start()
    {
        targetLane = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameStats.gameState != GameState.Play)
            return;
        getLane();
        move();
    }

    public void getLane()
    {
        if (Input.GetKeyDown("right"))
        {
            //move right
            targetLane++;
            if (targetLane == 2)
                targetLane = 1;
        }
        else if (Input.GetKeyDown("left"))
        {
            //move left
            targetLane--;
            if (targetLane == -2)
                targetLane = -1;
        }
    }

    public void move()
    {
        targetPosition = transform.position;

        targetPosition.x = targetLane * LANE_DISTANCE;

        //move from current position to target position
        transform.position = Vector3.Lerp(transform.position, targetPosition, 0.5f);
    }
}
