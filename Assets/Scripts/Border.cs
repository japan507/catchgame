﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Border : MonoBehaviour
{

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            FindObjectOfType<GameStats>().missCatch();
            FindObjectOfType<LivesPanel>().lostLife();
            Destroy(collision.gameObject);
        }

    }
}
