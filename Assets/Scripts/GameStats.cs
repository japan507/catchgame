﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public enum GameState
{
    Play,
    Result
}

public class ShowUI: UnityEvent<string> { }

public class GameStats : MonoBehaviour
{
    [SerializeField]
    private int lives;

    private int score;

    public static GameState gameState;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
        gameState = GameState.Play;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameState == GameState.Result)
        {
            if (Input.GetKeyDown("r"))
                restart();

        }
    }

    public int getScore()
    {
        return score;
    }

    public int getLives()
    {
        return lives;
    }

    public void updateScore(int score)
    {
        this.score += score;
    }

    //decrease live as player catches wrong object or miss
    public void missCatch()
    {
        lives--;
        if(lives == 0)
        {
            gameOver();
        }
    }
    public void gameOver()
    {
        gameState = GameState.Result;
        //show gameover text
        FindObjectOfType<CanvasController>().uiSetActive("GameOver", true);
    }
    public void restart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    
}
