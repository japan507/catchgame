﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum BucketColor
{
    Yellow,
    Red,
    Green
}
public class CatchObject : MonoBehaviour
{
    [SerializeField]
    private BucketColor color;

    [SerializeField]
    private GameStats gameStats;

    [SerializeField]
    private Spawner spawner;

    [SerializeField]
    private LivesPanel livesPanel;

    private SpriteRenderer renderer;

    private BoxCollider2D collider;


    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        color = BucketColor.Yellow;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameStats.gameState != GameState.Play)
            return;
        if (Input.GetKeyDown("space"))
            toggleColor();
    }

    public void toggleColor()
    {
        switch (color)
        {
            case BucketColor.Yellow:
                color = BucketColor.Red;
                renderer.color = new Color(1, 0, 0);
                break;
            case BucketColor.Red:
                color = BucketColor.Green;
                renderer.color = new Color(0, 1, 0);
                break;
            case BucketColor.Green:
                color = BucketColor.Yellow;
                renderer.color = new Color(1, 1, 1);
                break;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            Object obj = collision.GetComponent<Object>();
            if (obj.getColor() == color)
                gameStats.updateScore(obj.getScore() * spawner.getScoreMultiplier());
            else
            {
                FindObjectOfType<GameStats>().missCatch();
                FindObjectOfType<LivesPanel>().lostLife();
            }
            Destroy(collision.gameObject);
        }
    }
}
