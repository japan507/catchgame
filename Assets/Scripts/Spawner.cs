﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level{
    public float spawnInterval;
    public int scoreMultiplier;
}
public class Spawner : MonoBehaviour
{
    [SerializeField]
    private List<Level> level;

    private int currentLevel;

    [SerializeField]
    private float nextLevelCountDown;

    private float playTime;
    private float spawnCountDown;

    private int[] lanes = { -1, 0, 1 };

    private int i;

    [SerializeField]
    private List<Transform> objects;


    // Start is called before the first frame update
    void Start()
    {
        i = 0;
        playTime = 0;
        currentLevel = 0;
        spawnCountDown = level[currentLevel].spawnInterval;
        shuffle();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameStats.gameState != GameState.Play)
            return;

        playTime += Time.deltaTime;
        //increase level at certain time
        if(playTime >= nextLevelCountDown)
        {
            playTime = 0;
            currentLevel++;
        }
        if(spawnCountDown > 0)
        {
            spawnCountDown -= Time.deltaTime;
        }
        else
        {
            //random lane
            Vector3 location = new Vector3(lanes[i] * 2, transform.position.y);
            i++;
            if (i == 3)
            {
                shuffle();
                i = 0;
            }
            //random object
            Transform spawnObject = objects[Random.Range(0, objects.Count)];
            //spawn
            Instantiate(spawnObject, location, Quaternion.identity);
            
            spawnCountDown = level[currentLevel].spawnInterval;
        }
    }

    public void shuffle()
    {
        for (int i = 0; i < lanes.Length; i++)
        {
            int rnd = Random.Range(0, lanes.Length);
            int temp = lanes[rnd];
            lanes[rnd] = lanes[i];
            lanes[i] = temp;
        }
    }

    public int getScoreMultiplier()
    {
        return level[currentLevel].scoreMultiplier;
    }
}
